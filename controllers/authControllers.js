const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const catchAsync = require('../utils/catchAsync');
const User = require('../models/userModel');
const sendEmail = require('../utils/sendEmail');
require('dotenv').config({ path: '../config.env' });

exports.registerUser = catchAsync(async (req, res) => {
  const { email, password, role } = req.body;
  if (!email || !password || !role) {
    return res.status(400).json({
      message: 'Provide email, password and role',
    });
  }

  const existingUser = await User.findOne({ email: email });
  if (existingUser) {
    return res.status(400).json({
      message: 'User with this email already exists!',
    });
  }
  await User.create({
    email: email,
    password: await bcrypt.hash(password, 10),
    role: role,
  });

  res.status(200).json({
    message: 'Profile created successfully',
  });
});

exports.loginUser = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({
      message: 'Please provide password and email',
    });
  }

  const user = await User.findOne({ email }).select('+password');

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({
      message: 'Incorrect email or password',
    });
  }

  const token = jwt.sign(
    { id: user._id, email: user.email, role: user.role },
    process.env.JWT_SECRET
  );

  req.user = user;

  res.status(200).json({
    jwt_token: token,
  });
});

exports.forgottenPassword = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    return res.status(400).json({
      message: 'No user with such email found',
    });
  }

  const temporaryPassword = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '')
    .substr(0, 6);

  user.password = await bcrypt.hash(temporaryPassword, 10);
  await user.save({
    validateBeforeSave: false,
  });

  const emailOptions = {
    email: user.email,
    subject: 'Reset Password',
    message: `Temporary password: ${temporaryPassword}\n Log In your account with this password and update your password!`,
  };
  sendEmail(emailOptions);

  res.status(200).json({
    message: 'New password sent to your email address',
  });
};
